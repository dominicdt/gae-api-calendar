import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.client.util.DateTime;

@WebServlet(
		name = "OAuth2Callback",
		urlPatterns = {"/oauth2callback"}
		)

public class OAuth2Callback  extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
		String clientId = "";
		String clientSecret = "";
		String originURI = "";
		String redirectUri = "";
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		

		
		// Load your ClientId and ClientSecret from the clientIdAndSecret text file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("clientIdAndSecret.txt").getFile());
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			clientId = reader.readLine();
			clientSecret = reader.readLine();
			originURI = reader.readLine();
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		redirectUri = originURI + "/oauth2callback";
		
		// Establish the scope of the OAUTH request, more API's can be added as required
		Set<String> scope = Collections.singleton(CalendarScopes.CALENDAR_READONLY);

		// Create the Auth Code Flow
		AuthorizationCodeFlow.Builder codeFlowBuilder = 
				new GoogleAuthorizationCodeFlow.Builder(
						httpTransport, 
						jsonFactory, 
						clientId, 
						clientSecret, 
						scope
						);

		AuthorizationCodeFlow codeFlow = codeFlowBuilder.build();
		AuthorizationCodeRequestUrl authorizationUrl = codeFlow.newAuthorizationUrl();
		authorizationUrl.setRedirectUri(redirectUri);

		String[] s = request.getParameterValues("code");
		String code = s[0];

		AuthorizationCodeTokenRequest tokenRequest = codeFlow.newTokenRequest(code);
		tokenRequest.setRedirectUri(redirectUri);
		TokenResponse tokenResponse = tokenRequest.execute();

		// Use the OAUTH code to build a credential
		com.google.api.client.auth.oauth2.Credential credential = codeFlow.createAndStoreCredential(tokenResponse, "user");

		// Apply Credential to Calendar Service 
		Calendar calendar = new com.google.api.services.calendar.Calendar.Builder(
				httpTransport, jsonFactory, credential)
				.setApplicationName("GAE-API-CALENDAR")
				.build();

		// Query the calendar api for the next 5 calendar events from the primary calendar
		DateTime now = new DateTime(System.currentTimeMillis());
		List<Event> items = calendar.events().list("primary")
				.setMaxResults(5)
				.setTimeMin(now)
				.setOrderBy("startTime")
				.setSingleEvents(true)
				.execute().getItems();
		
		// Output results to page
		for(Event e : items) {
			if(e.getStart().getDate() == null) {
				response.getWriter().println(e.getSummary() + " - " + e.getStart().getDateTime());
			} else {
				response.getWriter().println(e.getSummary() + " - " + e.getStart().getDate());
			}
		}
	}

}
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@WebServlet(
		name = "OAuthRequest",
		urlPatterns = {"/OAuthRequest"}
		)
public class OAuthRequest extends HttpServlet{

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
		String clientId = "";
		String clientSecret = "";
		String originURI = "";
		String redirectUri = "";
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		// Load your ClientId and ClientSecret from the clientIdAndSecret text file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("clientIdAndSecret.txt").getFile());
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			clientId = reader.readLine();
			clientSecret = reader.readLine();
			originURI = reader.readLine();
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		redirectUri = originURI + "/oauth2callback";
		
		// Establish the scope of the OAUTH request, more API's can be added as required
		Set<String> scope = Collections.singleton(CalendarScopes.CALENDAR_READONLY);

		// Create the Auth Code Flow
		AuthorizationCodeFlow.Builder codeFlowBuilder = 
				new GoogleAuthorizationCodeFlow.Builder(
						httpTransport, 
						jsonFactory, 
						clientId, 
						clientSecret, 
						scope
						);
		AuthorizationCodeFlow codeFlow = codeFlowBuilder.build();
		AuthorizationCodeRequestUrl authorizationUrl = codeFlow.newAuthorizationUrl();
		authorizationUrl.setRedirectUri(redirectUri);
		
		// Redirects user to the Google Authentication page.
		response.sendRedirect(authorizationUrl.build());
	}
}
